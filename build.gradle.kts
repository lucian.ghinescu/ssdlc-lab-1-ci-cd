

plugins {
    id("java")
    id("jacoco")
//    id("com.github.spotbugs") version "6.0.27"
//    id("pmd")

}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
//    implementation("com.github.spotbugs:spotbugs:4.7.3") // Use the latest version
//    spotbugsPlugins("com.h3xstream.findsecbugs:findsecbugs-plugin:1.12.0")
//    implementation("org.ow2.asm:asm:9.5") // Replace with the latest version if newer

    // Include PMD as a dependency for compiling custom rules
//    implementation ("net.sourceforge.pmd:pmd-java:6.39.0")
}





tasks.jar {
    manifest {
        attributes(
            "Main-Class" to "org.example.Main"  // Replace with the fully qualified name of your main class
        )
    }
}

jacoco {
    toolVersion = "0.8.10" // Set the desired JaCoCo version
}

tasks.test {
    useJUnitPlatform()
    // Ensure JaCoCo test coverage report runs after tests
    finalizedBy(tasks.jacocoTestReport)
//    finalizedBy(tasks.spotbugsMain)
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // Run tests before generating the report

    reports {
        xml.required.set(true) // Enable XML report for CI/CD
        html.required.set(true) // Enable HTML report for easy viewing
        csv.required.set(false) // Disable CSV report (optional)
    }
}

tasks.jacocoTestCoverageVerification {
    dependsOn(tasks.jacocoTestReport)

    violationRules {
        rule {
            limit {
                minimum = 0.6.toBigDecimal() // Example threshold: 80% minimum coverage
            }
        }
    }
}



//// require Gradle 8.2+
//tasks.spotbugsMain {
//    reports.create("html") {
//        required = true
//        outputLocation = file("$buildDir/reports/spotbugs.html")
//        setStylesheet("fancy-hist.xsl")
//    }
//}
//spotbugs {
//    toolVersion.set("4.8.6") // Specify SpotBugs tool version
//    effort.set(com.github.spotbugs.snom.Effort.MAX) // Effort level (MIN, DEFAULT, MAX)
////    reportLevel.set(com.github.spotbugs.snom.ReportLevel.LOW) // Report level (LOW, MEDIUM, HIGH)
//}

//configure<PmdExtension> {
//    toolVersion = "6.39.0"
//    ruleSets = listOf(
////        "category/java/bestpractices.xml",
////        "category/java/errorprone.xml",
////        "C:\\Users\\Ghinescu Lucian\\Desktop\\Master\\Sem3\\SSDLC\\Project code\\ssdlc-lab-1-ci-cd\\src\\main\\resources\\pmd\\customRuleset.xml"
//"C:\\Users\\Ghinescu Lucian\\Desktop\\Master\\Sem3\\SSDLC\\Project code\\ssdlc-lab-1-ci-cd\\src\\main\\java\\org\\example\\customRuleset.xml"
//    )
//    isConsoleOutput = true // Optionally enable console output
//}

//
//tasks.withType<SpotBugsTask> {
//    reports.create("html") {
//        required = true
//        outputLocation = file("$buildDir/reports/spotbugs.html")
//        setStylesheet("fancy-hist.xsl")
//    }
//}

//pmd {
//    toolVersion = "7.9.0"  // Ensure this is the latest or a version that supports Java 17
//}
//
//tasks {
//    withType<org.gradle.api.plugins.quality.Pmd> {
//        reports {
//            html.required= true
//            xml.required = false // Disable XML if not needed
//        }
//    }
//    named<org.gradle.api.plugins.quality.Pmd>("pmdMain") {
//        ignoreFailures = false
//    }
//}
tasks.jar {
    from(sourceSets.main.get().output)
    from(sourceSets.main.get().resources)
    manifest {
        attributes(
            "Plugin-Id" to "PrintlnDetector",
            "Plugin-Version" to "1.0",
            "Plugin-Provider" to "Example Company",
            "Detector-Classes" to "com.example.PrintlnDetector"
        )
    }
}

