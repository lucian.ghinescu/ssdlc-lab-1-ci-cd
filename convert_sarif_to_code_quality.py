import json

# Load the SARIF report
with open("gl-sast-report.json", "r") as sarif_file:
    sarif_data = json.load(sarif_file)

# Prepare Markdown content
markdown_report = ["# Semgrep Analysis Report", ""]

for run in sarif_data.get("runs", []):
    for result in run.get("results", []):
        message = result["message"]["text"]
        rule_id = result.get("ruleId", "N/A")
        severity = result.get("properties", {}).get("severity", "N/A")

        # Extract file location
        locations = result.get("locations", [])
        for location in locations:
            physical_location = location.get("physicalLocation", {})
            file_path = physical_location.get("artifactLocation", {}).get("uri", "Unknown file")
            region = physical_location.get("region", {})
            start_line = region.get("startLine", "Unknown line")
            markdown_report.append(f"- **{file_path}:{start_line}**: `{rule_id}` ({severity}) - {message}")

# Write the Markdown file
with open("summary.markdown", "w") as markdown_file:
    markdown_file.write("\n".join(markdown_report))

print("SARIF report converted to Markdown!")
