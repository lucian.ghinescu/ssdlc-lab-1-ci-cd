
import org.example.Main;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MainTest {

    @Test
    public void testIsEven() {
        assertTrue(Main.isEven(2), "2 should be even");
        assertFalse(Main.isEven(3), "3 should be odd");
    }

    @Test
    public void testFactorial() {
        assertEquals(1, Main.factorial(0), "Factorial of 0 should be 1");
        assertEquals(1, Main.factorial(1), "Factorial of 1 should be 1");
        assertEquals(120, Main.factorial(5), "Factorial of 5 should be 120");
    }

    @Test
    public void testFactorialNegativeNumber() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Main.factorial(-1);
        });
        assertEquals("Number must be non-negative", exception.getMessage());
    }
}