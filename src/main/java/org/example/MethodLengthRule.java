//package org.example;
//
//import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
//import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
//
//public class MethodLengthRule extends AbstractJavaRule {
//
//    private static final int MAX_LENGTH = 10;  // Maximum allowed lines in a method
//
//    @Override
//    public Object visit(ASTMethodDeclaration node, Object data) {
//        int lines = node.getEndLine() - node.getBeginLine();
//
//        if (lines > MAX_LENGTH) {
//            addViolationWithMessage(data, node,
//                    "Method exceeds maximum allowed length of " + MAX_LENGTH + " lines.");
//        }
//
//        return super.visit(node, data);
//    }
//}