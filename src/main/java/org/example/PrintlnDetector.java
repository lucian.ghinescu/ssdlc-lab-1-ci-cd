//package org.example;
//
//
//
//import edu.umd.cs.findbugs.BytecodeScanningDetector;
//import edu.umd.cs.findbugs.BugInstance;
//import edu.umd.cs.findbugs.BugReporter;
//import org.apache.bcel.Const;
//
//public class PrintlnDetector extends BytecodeScanningDetector {
//    private final BugReporter reporter;
//
//    public PrintlnDetector(BugReporter reporter) {
//        this.reporter = reporter;
//    }
//
//    @Override
//    public void sawOpcode(int seen) {
//        if (seen == Const.INVOKEVIRTUAL && getClassConstantOperand().equals("java/io/PrintStream")
//                && getNameConstantOperand().equals("println")) {
//            reporter.reportBug(new BugInstance(this, "PRINTLN_USED", HIGH_PRIORITY)
//                    .addClassAndMethod(this)
//                    .addSourceLine(this));
//        }
//    }
//}
//
