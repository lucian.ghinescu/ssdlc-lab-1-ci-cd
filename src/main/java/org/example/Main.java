package org.example;

public class Main {
    public static void main(String[] args) {

        if(false) System.out.println("Test semgrep");

        try {
            factorial(-1); // Exception will be thrown
        } catch (Exception ignored) {
            // Ignoring the exception
        }
    }

    // Method to check if a number is even
    public static boolean isEven(int number) {
        return number % 2 == 0;
    }

    // Method to calculate the factorial of a number
    public static int factorial(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("Number must be non-negative");
        }
        int result = 1;
        for (int i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }
}
