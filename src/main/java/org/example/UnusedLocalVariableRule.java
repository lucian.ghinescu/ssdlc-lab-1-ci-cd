//package org.example;
//
//import net.sourceforge.pmd.lang.java.ast.ASTLocalVariableDeclaration;
//import net.sourceforge.pmd.lang.java.ast.ASTVariableDeclaratorId;
//import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
//import net.sourceforge.pmd.lang.java.symboltable.VariableNameDeclaration;
//import net.sourceforge.pmd.lang.symboltable.NameDeclaration;
//import net.sourceforge.pmd.lang.symboltable.NameOccurrence;
//import net.sourceforge.pmd.lang.symboltable.Scope;
//
//import java.util.List;
//import java.util.Map;
//
//public class UnusedLocalVariableRule extends AbstractJavaRule {
//
//    public UnusedLocalVariableRule() {
//        super();
//    }
//    @Override
//    public Object visit(ASTLocalVariableDeclaration node, Object data) {
//        List<ASTVariableDeclaratorId> vars = node.findDescendantsOfType(ASTVariableDeclaratorId.class);
//        for (ASTVariableDeclaratorId var : vars) {
//            Scope scope = var.getScope();
//            Map<NameDeclaration, List<NameOccurrence>> variableMap = scope.getDeclarations();
//            boolean used = false;
//
//            for (Map.Entry<NameDeclaration, List<NameOccurrence>> entry : variableMap.entrySet()) {
//                NameDeclaration decl = entry.getKey();
//                if (decl.getName().equals(var.getImage())) {
//                    List<NameOccurrence> occurrences = entry.getValue();
//                    if (!occurrences.isEmpty()) {
//                        used = true;
//                        break;
//                    }
//                }
//            }
//
//            if (!used) {
//                addViolation(data, var);
//            }
//        }
//        return super.visit(node, data);
//    }
//
//   }